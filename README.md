# Driver

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]
[![Total Downloads][ico-downloads]][link-downloads]

Get direct url from google drive


## Install

Via Composer

``` bash
$ composer require fwdmo/driver
```

## Usage

``` php
$video = new Fwdmo\Driver({video_id});

echo $video->get()->name // Video name;
echo $video->get()->url // Video url;
echo $video->get()->size // Video size;
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.

## Security

If you discover any security related issues, please email fwdmota@gmail.com instead of using the issue tracker.

## Credits

- [Fwdmo][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/fwdmo/driver.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/fwdmo/driver/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/fwdmo/driver.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/fwdmo/driver.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/fwdmo/driver.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/fwdmo/driver
[link-travis]: https://travis-ci.org/fwdmo/driver
[link-scrutinizer]: https://scrutinizer-ci.com/g/fwdmo/driver/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/fwdmo/driver
[link-downloads]: https://packagist.org/packages/fwdmo/driver
[link-author]: https://github.com/fwdmo
[link-contributors]: ../../contributors