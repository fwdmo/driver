<?php
namespace Fwdmo;

use GuzzleHttp\Client;

class Driver
{
    /** @var array Id key of video */
    private $id_key;

    public function __construct($id_key)
    {
        $this->id_key = $id_key;
        $this->client = new Client();
    }

   /**
    * Send request to google drive
    *
    * @return string
    */
    private function request()
    {
        // add ID of video to url
        $url = "https://drive.google.com/uc?id={$this->id_key}&confirm=jYel&authuser=0&export=download";

        // Add headers to request
        $res = $this->client->request("POST",$url, [
        'headers' => [
            'Accept' => '*/*',
            'Accept-encoding' => 'gzip, deflate, br',
            'Accept-language' => 'en-GB,en-US;q=0.9,en;q=0.8',
            'Content-length' => '0',
            'Content-type' => 'application/x-www-form-urlencoded;charset=UTF-8',
            'Origin' => 'https://drive.google.com',
            'Referer' => 'https://drive.google.com/drive/my-drive',
            'User-agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',
            'X-chrome-connected' => 'id=102224796319835333482,mode=0,enable_account_consistency=false',
            'X-cligent-data' => 'CIa2yQEIpbbJAQipncoBCKijygEYkqPKAQ==',
            'X-drive-first-party' => 'DriveWebUi',
            'X-json-requested' => 'true',
          ]
        ]);
        return $res->getBody();
    }

   /**
    * Serialize request body into array
    *
    * @return array $convert
    */
    private function convert()
    {
        // Decode request body
        $convert = json_decode(str_replace(')]}\'', '', $this->request()));

        return (object) [
             'url' => $convert->downloadUrl,
             'name' => $convert->fileName,
             'size' => $convert->sizeBytes,
        ];
    }


    public function get()
    {
        return $this->convert();
    }
}